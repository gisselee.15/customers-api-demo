package com.techprimers.springboot.swaggerexample.service;

import java.util.List;

import com.techprimers.springboot.swaggerexample.model.Client;

public interface IClientService {
	 List<Client> findAll();
	 Client findById(int idClient);
	 Client save(Client client);
	 void deleteById(int idClient);
}