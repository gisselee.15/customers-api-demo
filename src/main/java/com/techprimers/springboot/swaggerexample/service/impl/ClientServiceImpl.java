package com.techprimers.springboot.swaggerexample.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techprimers.springboot.swaggerexample.model.Client;
import com.techprimers.springboot.swaggerexample.repository.IClientRepository;
import com.techprimers.springboot.swaggerexample.service.IClientService;

@Service
public class ClientServiceImpl implements IClientService {

	@Autowired
	private IClientRepository clientRepository;
	
	@Override
	public List<Client> findAll() {
		return clientRepository.findAll();
	}


	@Override
	public Client save(Client client) {
		return clientRepository.save(client);
	}


	@Override
	public Client findById(int idClient) {
		return clientRepository.findOne(idClient);
	}


	@Override
	public void deleteById(int idClient) {
		clientRepository.delete(idClient);
		
	}

	
}