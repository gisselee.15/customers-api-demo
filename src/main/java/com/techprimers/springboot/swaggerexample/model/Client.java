package com.techprimers.springboot.swaggerexample.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;



@Entity
@Table(name = "client")
public class Client  implements Serializable {

	private static final long serialVersionUID = 5340730744364573534L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull(message = "El campo Cédula de identidad es un campo requerido")
	private Integer cedulaIdentidad;
	
	@NotNull(message = "El campo Nombre es un campo requerido")
	private String nombres;
	
	@NotNull(message = "El campo Apellido es un campo requerido")
	private String apellidos;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCedulaIdentidad() {
		return cedulaIdentidad;
	}

	public void setCedulaIdentidad(Integer cedulaIdentidad) {
		this.cedulaIdentidad = cedulaIdentidad;
	}


	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public void setIdClient(int idClient) {
		// TODO Auto-generated method stub
		
	}
}