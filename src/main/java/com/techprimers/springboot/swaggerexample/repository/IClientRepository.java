package com.techprimers.springboot.swaggerexample.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techprimers.springboot.swaggerexample.model.Client;


@Repository
public interface IClientRepository extends JpaRepository<Client, Integer> {

}